﻿/*global module:false*/
module.exports = function (grunt) {
    
    // Project configuration.
    grunt.initConfig({
        // Metadata.
        pkg: grunt.file.readJSON('package.json'),
        banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
      ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
        // Task configuration.
        uglify: {
            options: {
                banner: '<%= banner %>'
            },
            dist: {
                options: {
                    sourceMap: true,
                },
                files: [{
                        expand: true,
                        cwd: 'client/',
                        src: '*.js',
                        dest: 'public/js/',
                        ext: '.min.js',
                        extDot: 'first'
                    }]
            }
        },
        copy: {
            toPublic: {
                files: [
          // includes files within path
                    { expand: true, cwd: 'bin/', src: ['client/rtsp.io-client-js.min.js', 'rtsp.io-client-js.map'], dest: 'public/javascripts/lib/', filter: 'isFile' }
                ]
            }
        }
    });
    
    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    
    // Default task.
    grunt.registerTask('default', ['uglify']);
    grunt.registerTask('build', ['uglify']);

};