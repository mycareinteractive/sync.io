﻿(function ($) {
    
    var toStr = function (obj) {
        return JSON.stringify(obj);
    };
    
    var Sender = {
        io: null,
        socket: null,
        
        serverUrl: '',
        uid: '',
        ip: '',
        device: '',
        
        events: {
            connected: 'syncio.channel.connected',
            disconnect: 'syncio.channel.disconnect',
        },
        
        init: function (obj) {
            console.log('Initialize sender client');
            var self = this;
            for (var o in obj) {
                this[o] = obj[o];
            }
            
            // only get socket.io js once
            if (!window.io) {
                var url = this.serverUrl + '/socket.io/socket.io.js';
                console.log('loading socket.io lib from ' + url);
                $.ajax({
                    url: url,
                    crossDomain: true,
                    dataType: "script"
                }).done(function () {
                    console.log('socket.io lib loaded');
                    self.io = window.io;
                    self.connect();
                });
            }
            else {
                console.log('socket.io lib was already loaded previously');
                self.io = window.io;
                self.connect();
            }
        },
        
        connect: function () {
            console.log('sender connect');
            
            this.socket = this.io(this.serverUrl + '/channel', { forceNew: true, query: 'role=sender&uid=' + this.uid });
            this.initListeners();
            if (!this.socket.connected) {
                this.socket.connect(this.serverUrl);
                console.log('connecting to ' + this.serverUrl);
            }
        },
        
        disconnect: function () {
            if (this.socket) {
                this.socket.off('message');
                this.socket.disconnect();
                delete this.socket;
                this.socket = null;
                this.url = null;
                console.log('sender disconnected!');
            }
        },
        
        navigate: function (obj) {
            console.log('navigate ', toStr(obj));
            this.socket.emit('sender.navigate', obj);
        },
        
        click: function (obj) {
            console.log('click ', toStr(obj));
            this.socket.emit('sender.click', obj);
        },
        
        key: function (obj) {
            console.log('key ', toStr(obj));
            this.socket.emit('sender.key', obj);
        },
        
        initListeners: function () {
            var self = this;
            
            // socket.io 'io' object events
            this.socket.once('connect', function () {
                console.log('sender connected successfully!');
                $(document).trigger(self.events.connected);
            });
            
            this.socket.once('disconnect', function () {
                console.log('sender disconnected!');
                $(document).trigger(self.events.disconnected);
            });
            
            this.socket.on('message', function (data) {
                console.log(data);
            });
            
            this.socket.on('connect_error', function (e) {
                console.log('sender connection error! ' + e);
            });
            
            this.socket.on('reconnect', function () {
                console.log('sender re-connected successfully!');
            });
            
            this.socket.on('reconnecting', function () {
                console.log('sender re-connecting...');
            });
            
            this.socket.on('reconnect_error', function (e) {
                console.log('sender re-connection error! ' + e);
            });
            
            // sync.io custom events
            this.socket.on('receiver.join', function (uid) {
                console.log('receiver joined ', uid);
            });
            
        },
    };
    
    window.syncio = window.syncio || {};
    window.syncio.channel = window.syncio.channel || {};
    window.syncio.channel.sender = Sender;

}(jQuery));