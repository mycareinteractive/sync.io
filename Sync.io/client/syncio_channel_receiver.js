﻿(function ($) {
    
    var toStr = function (obj) {
        return JSON.stringify(obj);
    };
    
    var Receiver = {
        io: null,
        socket: null,
        
        serverUrl: '',
        uid: '',
        ip: '',
        device: '',
        
        events: {
            connected: 'syncio.channel.connected',
            disconnect: 'syncio.channel.disconnect',
            navigate: 'syncio.channel.navigate',
            click: 'syncio.channel.click',
            key: 'syncio.channel.key'
        },
        
        init: function (obj) {
            console.log('Initialize receiver client');
            var self = this;
            for (var o in obj) {
                this[o] = obj[o];
            }
            
            // only get socket.io js once
            if (!window.io) {
                var url = this.serverUrl + '/socket.io/socket.io.js';
                console.log('loading socket.io lib from ' + url);
                $.ajax({
                    url: url,
                    crossDomain: true,
                    dataType: "script"
                }).done(function () {
                    console.log('socket.io lib loaded');
                    self.io = window.io;
                    self.connect();
                });
            }
            else {
                console.log('socket.io lib was already loaded previously');
                self.io = window.io;
                self.connect();
            }
        },
        
        connect: function () {
            console.log('receiver connect');
            
            this.socket = this.io(this.serverUrl + '/channel', { forceNew: true, query: 'role=receiver&uid=' + this.uid });
            this.initListeners();
            if (!this.socket.connected) {
                this.socket.connect(this.serverUrl);
                console.log('connecting to ' + this.serverUrl);
            }
        },
        
        disconnect: function () {
            if (this.socket) {
                this.socket.off('message');
                this.socket.disconnect();
                delete this.socket;
                this.socket = null;
                this.url = null;
                console.log('receiver disconnected!');
            }
        },

        initListeners: function () {
            var self = this;
            
            // socket.io 'io' object events
            this.socket.once('connect', function () {
                console.log('receiver connected successfully!');
                $(document).trigger(self.events.connected);
            });
            
            this.socket.once('disconnect', function () {
                console.log('receiver disconnected!');
                $(document).trigger(self.events.disconnected);
            });
            
            this.socket.on('message', function (data) {
                console.log(data);
            });
            
            this.socket.on('connect_error', function (e) {
                console.log('receiver connection error! ' + e);
            });
            
            this.socket.on('reconnect', function () {
                console.log('receiver re-connected successfully!');
            });
            
            this.socket.on('reconnecting', function () {
                console.log('receiver re-connecting...');
            });
            
            this.socket.on('reconnect_error', function (e) {
                console.log('receiver re-connection error! ' + e);
            });
            
            // sync.io custom events
            this.socket.on('sender.navigate', function (obj) {
                console.log('sender navigate ', toStr(obj));
                $(document).trigger(self.events.navigate, obj);
            });

            this.socket.on('sender.click', function (obj) {
                console.log('sender click ', toStr(obj));
                $(document).trigger(self.events.click, obj);
            });

            this.socket.on('sender.key', function (obj) {
                console.log('sender key ', toStr(obj));
                $(document).trigger(self.events.key, obj);
            });
            
        },
    };
    
    window.syncio = window.syncio || {};
    window.syncio.channel = window.syncio.channel || {};
    window.syncio.channel.receiver = Receiver;

}(jQuery));