﻿function Receiver(io, socket, uid) {
    this.io = io;
    this.socket = socket;
    this.uid = uid;

    // messages from clients
    this.socketHandler = {
    };
    
    // messages to clients
    this.events = {
        join: 'receiver.join'    // to sender
    };

    // room eg: 100100A.receivers
    this.socket.join(uid + '.receivers');
    this.senders(uid).emit(this.events.join, uid);
}

Receiver.prototype.senders = function () {
    return this.io.to(this.uid + '.senders');
};

Receiver.prototype.receivers = function () {
    return this.io.to(this.uid + '.receivers');
};

// receiver join events will be published to senders
Receiver.prototype.destroy = function () {
};

Receiver.prototype.toStr = function (obj) {
    return 'uid=' + this.uid + ' ' + JSON.stringify(obj);
};

module.exports = Receiver;