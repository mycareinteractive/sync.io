﻿function Sender(io, socket, uid) {
    this.io = io;
    this.socket = socket;
    this.uid = uid;
    
    // messages from clients
    this.socketHandler = {
        'sender.navigate': this.navigate,
        'sender.click': this.click,
        'sender.key': this.key,
    };
    
    // messages to clients
    this.events = {
        join: 'sender.join',
        navigate: 'sender.navigate',
        click: 'sender.click',
        key: 'sender.key'
    };

    // join room eg: 100100A.senders
    this.socket.join(uid + '.senders');
    this.receivers(uid).emit(this.events.join, uid);
}

Sender.prototype.senders = function () {
    return this.io.to(this.uid + '.senders');
};

Sender.prototype.receivers = function () {
    return this.io.to(this.uid + '.receivers');
};

// sender join events will be published to receivers
Sender.prototype.navigate = function (obj) {
    console.log('sender navigate: ', this.toStr(obj));
    this.receivers().emit(this.events.navigate, obj);
};

Sender.prototype.click = function (obj) {
    console.log('sender click: ', this.toStr(obj));
    this.receivers().emit(this.events.click, obj);
};

Sender.prototype.key = function (obj) {
    console.log('sender key: ', this.toStr(obj));
    this.receivers().emit(this.events.key, obj);
};

Sender.prototype.destroy = function () {
};

Sender.prototype.toStr = function (obj) {
    return 'uid=' + this.uid + ' ' + JSON.stringify(obj);
};

module.exports = Sender;