﻿var http = require('http').createServer(handler)
  , io = require('socket.io')()
  , config = require('config')
  , debug = require('debug')
  , path = require('path')
  , fs = require('fs');

var Sender = require('./channel/sender.js');
var Receiver = require('./channel/receiver.js');


// http handler
function handler(req, res) {
    var queryPos = req.url.indexOf('?');
    var url = req.url;
    if (queryPos > 0)
        url = req.url.substr(0, queryPos);
    
    if (url.substr(0, 3) == '/js') {
        var file = 'public' + url;
        console.log(file);
        fs.readFile(path.resolve(__dirname, file), function (err, content) {
            if (err) {
                res.writeHead(500);
                return res.end('Error loading javascript: ' + file + '\n' + err);
            }
            res.setHeader("Content-Length", content.length);
            res.setHeader('Content-Type', 'application/javascript');
            res.statusCode = 200;
            res.end(content);
        });
    } else {
        var file = 'public' + url;
        if (file == 'public/')
            file = 'public/index.html';
        fs.readFile(path.resolve(__dirname, file), function (err, content) {
            if (err) {
                res.writeHead(500);
                return res.end('Error loading ' + file + '\n' + err);
            }
            res.setHeader("Content-Length", content.length);
            res.setHeader('Content-Type', 'text/html');
            res.statusCode = 200;
            res.end(content);
        });
    }
}

function initialize() {
    
    // Attach to http server and set socket.io options. (eg: transport method)
    // Change config/production.config to change it.
    if (config.has('socketio.options'))
        io.attach(http, config.get('socketio.options'));
    
    // Optional settings
    if (config.has('socketio.origins'))
        io.origins(config.get('socketio.origins'));
    
    io.set('origins', '*:*');
    
    if (process.env.IISNODE_VERSION) {
        // If this node.js application is hosted in IIS, assume it is hosted in IIS virtual directory named 'rtspio' and set up 
        // the socket.io 's path to recognize requests that target it. 
        // Note a corresponding change in the client index-socketio.html, as well as necessary URL rewrite rule in web.config. 
        //io.path(config.get('socketio.path'));
    }
    
    // Middleware goes here
    io.use(function (socket, next) {
        if (true) return next();
        next(new Error('Authentication error'));
    });
    
    
    
    // Register event handlers
    
    // "/channel" namespace    
    var channelIo = io.of('/channel');
    channelIo.on('connection', function (socket) {
        var socketId = socket.id;
        var clientIp = socket.request.connection.remoteAddress || socket.handshake.headers.host;
        var role = socket.handshake.query.role;
        var uid = socket.handshake.query.uid;
        console.log('[%s] %s connected to /channel %s.%s', clientIp, socketId, uid, role + 's');
        
        var client = null;
        if (role === 'sender') {
            client = new Sender(channelIo, socket, uid);
        }
        else if (role === 'receiver') {
            client = new Receiver(channelIo, socket, uid);
        }
        else {
            console.log('Unknown role:%s', role);
        }
        
        if (client) {
            // Bind all events to client
            for (var event in client.socketHandler) {
                socket.on(event, client.socketHandler[event].bind(client));
            }
        }

        socket.on('disconnect', function () {
            console.log('[%s] socket %s disconnected from /channel', clientIp, socketId);
        });

    });


}

initialize();
http.listen(process.env.PORT || 8889);