# Sync.io Server #

Sync.io is a Node.js web server that synchronizes real-time communication between different end-points such as TVs and tablets.

Build and Run
==============
There are two ways to run this server:

From Visual Studio
------------------
Just build and launch the app from Visual Studio, it will open a web browser.

From Command line
-----------------
Upon initial clone, you can run:

```
npm install
```

to install dependencies. Once done, you can run:

```
node app.js
```

and this will start a basic HTTP server that is accessed by going to [http://localhost:8889](http://localhost:8889) in your browser.

Once loaded in the browser, some instructions are given how to operate the test simulator.